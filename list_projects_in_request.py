#!/usr/bin/env python

import cPickle
from DIRAC.Core.Base.Script import parseCommandLine
parseCommandLine()
from DIRAC.Core.DISET.RPCClient import RPCClient
pr = RPCClient('ProductionManagement/ProductionRequest')
reqId = 46683
retVal  = pr.getProductionRequest([reqId])
prodDetail = cPickle.loads(retVal['Value'][reqId]['ProDetail'])

stepids = []
for i in range(9):
  stepid = prodDetail.get('p%dStep' % i)
  if stepid:
      stepids.append(int(stepid))

print(stepids)

from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient
bk = BookkeepingClient()


step_data = []

for step in stepids:
  retVal = bk.getAvailableSteps({'StepId':step})
  if not retVal['OK']:
     print retVal['Message']
  else:
    record = retVal['Value']['Records'][0]
    paramnames = record[-1]['ParameterNames']
    params = record[:-1]
    res = dict(zip(paramnames, params))
    t = ((res['ApplicationName'], res['ApplicationVersion'], res['SystemConfig']))
    step_data.append(t)


for t in step_data:
  print "_".join(( t[0].upper(), t[1], t[2].replace("-", "_")))
