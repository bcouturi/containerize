from ProdConf import ProdConf

ProdConf(
  NOfEvents=200,
  HistogramFile='Gauss_00071400_00000089_1.Hist.root',
  DDDBTag='dddb-20170721-3',
  AppVersion='v49r9',
  XMLSummaryFile='summaryGauss_00071400_00000089_1.xml',
  Application='Gauss',
  OutputFilePrefix='00071400_00000089_1',
  RunNumber=7140089,
  XMLFileCatalog='pool_xml_catalog.xml',
  FirstEventNumber=8801,
  CondDBTag='sim-20170721-2-vc-md100',
  OutputFileTypes=['sim'],
)
