

set(ENV{VS_UNICODE_OUTPUT} "")
set(command "./configure;--prefix=/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/install/MCGenerators/tauola++/1.1.6b.lhcb/x86_64-slc6-gcc48-opt;--with-pic;--with-hepmc=/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/install/HepMC/2.06.09/x86_64-slc6-gcc48-opt;--with-tau-spinner;--with-lhapdf=/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/install/MCGenerators/lhapdf/6.1.5.cxxstd/x86_64-slc6-gcc48-opt;CFLAGS=-O2;CPPFLAGS=-I/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/install/Boost/1.55.0_python2.7/x86_64-slc6-gcc48-opt/include/boost-1_55;FFLAGS=-O2;F77=/cvmfs/sft.cern.ch/lcg/contrib/gcc/4.8/x86_64-slc6/bin/gfortran")
execute_process(
  COMMAND ${command}
  RESULT_VARIABLE result
  OUTPUT_VARIABLE logs
  ERROR_VARIABLE logs
  )
file(WRITE "/mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/tauola++-1.1.6b.lhcb-stamp/tauola++-1.1.6b.lhcb-configure.log" ${logs} )
if(result)
  set(msg "Command failed: ${result}\n")
  foreach(arg IN LISTS command)
    set(msg "${msg} '${arg}'")
  endforeach()
  file(SHA1 "/mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/tauola++-1.1.6b.lhcb-stamp/tauola++-1.1.6b.lhcb-configure.log" sha1)
  set(msg "${msg}\nSee also\n  /mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/tauola++-1.1.6b.lhcb-stamp/tauola++-1.1.6b.lhcb-configure.log [${sha1}]\n")
  file(APPEND /mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/fail-logs.txt "/mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/tauola++-1.1.6b.lhcb-stamp/tauola++-1.1.6b.lhcb-configure.log\n")
  message(FATAL_ERROR "${msg}")
else()
  set(msg "tauola++-1.1.6b.lhcb configure command succeeded.  See also /mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/tauola++-1.1.6b.lhcb-stamp/tauola++-1.1.6b.lhcb-configure.log\n")
  message(STATUS "${msg}")
endif()
