message(STATUS "downloading...
     src='http://service-spi.web.cern.ch/service-spi/external/tarFiles/MCGeneratorsTarFiles/TAUOLA.1.1.6b.lhcb-LHC.tar.gz'
     dst='/mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/TAUOLA.1.1.6b.lhcb-LHC.tar.gz'
     timeout='none'")




file(DOWNLOAD
  "http://service-spi.web.cern.ch/service-spi/external/tarFiles/MCGeneratorsTarFiles/TAUOLA.1.1.6b.lhcb-LHC.tar.gz"
  "/mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/TAUOLA.1.1.6b.lhcb-LHC.tar.gz"
  SHOW_PROGRESS
  # no EXPECTED_HASH
  # no TIMEOUT
  STATUS status
  LOG log)

list(GET status 0 status_code)
list(GET status 1 status_string)

if(NOT status_code EQUAL 0)
  message(FATAL_ERROR "error: downloading 'http://service-spi.web.cern.ch/service-spi/external/tarFiles/MCGeneratorsTarFiles/TAUOLA.1.1.6b.lhcb-LHC.tar.gz' failed
  status_code: ${status_code}
  status_string: ${status_string}
  log: ${log}
")
endif()

message(STATUS "downloading... done")
