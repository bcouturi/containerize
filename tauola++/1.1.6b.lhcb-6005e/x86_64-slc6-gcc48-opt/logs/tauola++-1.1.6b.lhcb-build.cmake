

set(ENV{VS_UNICODE_OUTPUT} "")
set(command "${make}")
execute_process(
  COMMAND ${command}
  RESULT_VARIABLE result
  OUTPUT_VARIABLE logs
  ERROR_VARIABLE logs
  )
file(WRITE "/mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/tauola++-1.1.6b.lhcb-stamp/tauola++-1.1.6b.lhcb-build.log" ${logs} )
if(result)
  set(msg "Command failed: ${result}\n")
  foreach(arg IN LISTS command)
    set(msg "${msg} '${arg}'")
  endforeach()
  file(SHA1 "/mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/tauola++-1.1.6b.lhcb-stamp/tauola++-1.1.6b.lhcb-build.log" sha1)
  set(msg "${msg}\nSee also\n  /mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/tauola++-1.1.6b.lhcb-stamp/tauola++-1.1.6b.lhcb-build.log [${sha1}]\n")
  file(APPEND /mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/fail-logs.txt "/mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/tauola++-1.1.6b.lhcb-stamp/tauola++-1.1.6b.lhcb-build.log\n")
  message(FATAL_ERROR "${msg}")
else()
  set(msg "tauola++-1.1.6b.lhcb build command succeeded.  See also /mnt/build/jenkins/workspace/lcg_release_api/BUILDTYPE/Release/COMPILER/gcc48/LABEL/slc6/build/generators/tauola++-1.1.6b.lhcb/src/tauola++-1.1.6b.lhcb-stamp/tauola++-1.1.6b.lhcb-build.log\n")
  message(STATUS "${msg}")
endif()
