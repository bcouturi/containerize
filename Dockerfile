FROM centos:7 

# Basic system installation 

RUN yum -y install http://linuxsoft.cern.ch/wlcg/centos7/x86_64/wlcg-repo-1.0.0-1.el7.noarch.rpm 
RUN yum -y install epel-release 
RUN yum -y install python-pip  gcc HEP_OSlibs python-devel && yum clean all 

# Installing the LHCb base software 
RUN pip install --upgrade setuptools pip
RUN pip install lbinstall 
RUN lbinstall --root=/opt/lhcb install LBSCRIPTS CMT

# Installing needed LHCb projects
RUN lbinstall --root=/opt/lhcb install GAUSS_v49r9_x86_64_slc6_gcc48_opt
RUN lbinstall --root=/opt/lhcb install BOOLE_v30r2p1_x86_64_slc6_gcc49_opt
RUN lbinstall --root=/opt/lhcb install MOORE_v25r4_x86_64_slc6_gcc48_opt
RUN lbinstall --root=/opt/lhcb install MOORE_v25r4_x86_64_slc6_gcc48_opt
RUN lbinstall --root=/opt/lhcb install MOORE_v25r4_x86_64_slc6_gcc48_opt
RUN lbinstall --root=/opt/lhcb install BRUNEL_v50r2_x86_64_slc6_gcc49_opt
RUN lbinstall --root=/opt/lhcb install DAVINCI_v41r4p3_x86_64_slc6_gcc48_opt
RUN lbinstall --root=/opt/lhcb install DAVINCI_v41r4p4_x86_64_slc6_gcc49_opt
RUN lbinstall --root=/opt/lhcb install DBASE_Gen_DecFiles_v30r11
RUN lbinstall --root=/opt/lhcb install DBASE_AppConfig_v3r335
RUN lbinstall --root=/opt/lhcb install DBASE_ProdConf_v2r6

# The generators below *should* have been installed but there are missing dependencies
# on Gauss v49r9
RUN lbinstall --root=/opt/lhcb install LCG_79_pythia6_427.2_x86_64_slc6_gcc48_opt
RUN lbinstall --root=/opt/lhcb install LCG_79_photos++_3.56_x86_64_slc6_gcc48_opt 
RUN lbinstall --root=/opt/lhcb --nodeps install LCG_79_tauola++_1.1.6b.lhcb_x86_64_slc6_gcc48_opt
RUN mkdir -p /opt/lhcb/lcg/releases/MCGenerators/tauola++/1.1.6b.lhcb-6005e/x86_64-slc6-gcc48-opt
COPY tauola++/1.1.6b.lhcb-6005e/x86_64-slc6-gcc48-opt /opt/lhcb/lcg/releases/MCGenerators/tauola++/1.1.6b.lhcb-6005e/x86_64-slc6-gcc48-opt
RUN yum install -y  strace
RUN lbinstall --root=/opt/lhcb install LCG_79_pythia8_186_x86_64_slc6_gcc48_opt
RUN lbinstall --root=/opt/lhcb install LCG_79_lhapdf_6.1.4_x86_64_slc6_gcc48_opt

# Now preparing the script to be run
RUN mkdir /opt/containerize
COPY prodConf_Gauss_00071400_00000089_1.py /opt/containerize
COPY run.sh /opt/containerize
ENTRYPOINT /opt/containerize/run.sh
