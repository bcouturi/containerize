#!/bin/bash

filename=$(readlink -f  $BASH_SOURCE)
dir=$(dirname $filename)
LOC=$(cd  $dir;pwd)
export LOC

cd /tmp
source /opt/lhcb/LbLogin.sh
lb-run --use-grid -c x86_64-slc6-gcc48-opt  --use="AppConfig v3r335"  --use="DecFiles v30r11"  --use="ProdConf"   Gauss/v49r9 gaudirun.py -T '$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py' '$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py' '$APPCONFIGOPTS/Gauss/DataType-2016.py' '$APPCONFIGOPTS/Gauss/RICHRandomHits.py' '$DECFILESROOT/options/27163076.py' '$LBPYTHIA8ROOT/options/Pythia8.py' '$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py' '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py' '${LOC}/prodConf_Gauss_00071400_00000089_1.py'
