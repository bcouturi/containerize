# Containerize

Various tools to run a simulation workflow in a container.

## Building the container

You need docker configured on the machine, and just run:

```
./container_build.sh
```
## Running the container

```
./container_run.sh
```

You need docker installed.

See run.sh and prodConf_Gauss_00071400_00000089_1.py to find the options used.
